package main.java;

public class Start {
    public static void main(String[] args) {
        //создать новый экземпляр нельзя => TestSingleton ts =new TestSingleton();
        TestSingleton.getInstance().print();
        TestSingleton.getInstance().stop();
        TestSingleton.getInstance().print();
        TestSingleton.getInstance().stop();
    }
}
