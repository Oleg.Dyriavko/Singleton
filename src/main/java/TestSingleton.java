package main.java;

public class TestSingleton {
    //create var instance
    private static TestSingleton instance = new TestSingleton();

    //create function getInstance
    public static TestSingleton getInstance() {
        if (instance == null) {
            instance = new TestSingleton();
        }
        return instance;
    }

    //create closed constructor
    private TestSingleton() {
    }

    //create another function
    public void print() {
        System.out.println(this);

    }//create another function
    public void stop() {
        System.out.println("Stop");
    }
}
